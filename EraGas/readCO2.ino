int readCO2()
{
byte cmd[5] = {0x01, 0x20, 0x00, 0x39, 0xC0};
byte response[7]; 
  

Serial1.write(cmd, 5); //request PPM CO2
Serial.flush();
memset(response, 0, 7);
Serial.readBytes(response, 7);


  
  
  if (response[0] != 0x01)
  {
    Serial.println("Wrong starting byte from co2 sensor!");
    return -1;
  }

  if (response[1] != 0x20)
  {
    Serial.println("Wrong command from co2 sensor!");
    return -1;
  }

	byte responseHigh =  response[4];
	byte responseLow =   response[3];

int ppm = word(responseHigh,responseLow);
return ppm;
  

}
